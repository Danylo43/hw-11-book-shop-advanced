import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;

import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import * as sass from "sass";
import gulpSass from "gulp-sass";
// import imagemin from "imagemin";

import bsc from "browser-sync";
const browserSync = bsc.create();
const sassCompiler = gulpSass(sass);

const htmlTaskHandler = () => {
  return src("./src/*.html").pipe(dest("./dist"));
};

const cssTaskHandler = () => {
  return src("./src/scss/main.scss")
    .pipe(sassCompiler().on("error", sassCompiler.logError))
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(dest("./dist/css"))
    .pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
  return src("./src/images/**/*.*")
    .pipe(imagemin())
    .pipe(dest("./dist/images"));
};

const browserSyncTaskHandler = () => {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });

  watch("./src/scss/**/*.scss").on(
    "all",
    series(cssTaskHandler, browserSync.reload)
  );
  watch("./src/*.html").on(
    "change",
    series(htmlTaskHandler, browserSync.reload)
  );
  watch("./src/img/**/*").on(
    "all",
    series(imagesTaskHandler, browserSync.reload)
  );
};

const cleanDistTaskHandler = () => {
  return src("./dist", { allowEmpty: true, read: false }).pipe(clean());
};

// const image = () => {
//   return gulp
//     .src("./src/image/**/*.*")
//     .pipe(imagemin())
//     .pipe(gulp.dest("./dist/images"));
// };

const cleanDist = () => {
  return gulp.src("./dist", { allowEmpty: true }.pipe(clean()));
};

export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const images = imagesTaskHandler;

export const build = series(
  cleanDistTaskHandler,
  parallel(htmlTaskHandler, cssTaskHandler, imagesTaskHandler)
);
export const dev = series(build, browserSyncTaskHandler);
